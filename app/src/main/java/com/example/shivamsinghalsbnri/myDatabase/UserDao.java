package com.example.shivamsinghalsbnri.myDatabase;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.shivamsinghalsbnri.Item;

import java.util.List;

@Dao
public interface UserDao {
    @Query("SELECT * FROM Item")
    List<Item> getAll();

    @Query("SELECT COUNT(*) from Item")
    int countUsers();

    @Insert
    void insert(Item product);

    @Delete
    void delete(Item product);

    @Update
    void update(Item product);
}
