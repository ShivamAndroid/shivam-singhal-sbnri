package com.example.shivamsinghalsbnri.myDatabase;

import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.NonNull;

import com.example.shivamsinghalsbnri.Item;

import java.util.List;

public class DatabaseInitializer {


    private static final String TAG = DatabaseInitializer.class.getName();

    public static void populateAsync(@NonNull final AppDatabase db) {
        PopulateDbAsync task = new PopulateDbAsync(db);
        task.execute();
    }

    public static void populateSync(@NonNull final AppDatabase db) {
        populateWithTestData(db);
    }

    public static Item addProduct(final AppDatabase db, Item product) {
        db.userDao().insert(product);
        return product;
    }
    public static Item updateProduct(final AppDatabase db, Item product) {
        db.userDao().update(product);
        return product;
    }
    public static Item delete(final AppDatabase db, Item product) {
        db.userDao().delete(product);
        return product;
    }

    private static void populateWithTestData(AppDatabase db) {
        Item product = new Item();
        product.name = "Shivam Singhal";
        product.description = "Android Developer";
        product.id = 100;
        product.license_name = "AA";
        product.permissions = "ABC";
        product.open_issues_count = 1;
        addProduct(db, product);

        List<Item> productList = db.userDao().getAll();
        if (productList != null) {
            if (productList.size() > 0) {
                Log.d(DatabaseInitializer.TAG, "Rows Count: " + productList.size());
            }
        }
    }

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final AppDatabase mDb;

        PopulateDbAsync(AppDatabase db) {
            mDb = db;
        }

        @Override
        protected Void doInBackground(final Void... params) {
//            populateWithTestData(mDb);
            return null;
        }

    }
}
