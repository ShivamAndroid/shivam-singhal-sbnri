package com.example.shivamsinghalsbnri;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.shivamsinghalsbnri.myDatabase.AppDatabase;
import com.example.shivamsinghalsbnri.myDatabase.DatabaseInitializer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    RequestQueue queue = null;

    private RecyclerView rvItems = null;

    private EndlessRecyclerViewScrollListener scrollListener;

    List<Item> myList;

    int start = 0;

    CustomRecyclerViewAdapter adapter = null;

    private AppDatabase db = null;

    private ProgressDialog progressDialog = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, 101);

        db = AppDatabase.getAppDatabase(this);

        rvItems = findViewById(R.id.rvItems);


        // Hit for 1st api
        hitApiToFetchData(start,rvItems);

    }

    private void createFirstList(final int page) {

        final List<Item> list = new ArrayList<>();

        queue = Volley.newRequestQueue(MainActivity.this);

        String url = "https://api.github.com/orgs/octokit/repos?page="+page+"&per_page=10";

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e(TAG, "onResponse: " + response );

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    if (jsonArray !=  null){
                        if (jsonArray.length() > 0){
                            for (int i = 0; i < jsonArray.length(); i++){

                                Item item = new Item();

                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                item.id = jsonObject.getInt("id");
                                item.name = jsonObject.getString("name");
                                item.description = jsonObject.getString("description");
                                if (!jsonObject.isNull("license")) {
                                    item.license_name = (new JSONObject(jsonObject.getString("license"))).getString("name");
                                }else {
                                    item.license_name = "No license";
                                }

                                item.open_issues_count = jsonObject.getInt("open_issues_count");
                                item.permissions = parseForPermissions(new JSONObject(jsonObject.getString("permissions")));

                                list.add(item);

                            }

                            //Save Data to Room

                            List<Integer> idList = new ArrayList<>();
                            List<Item> dbList = db.userDao().getAll();
                            if (dbList != null) {
                                if (dbList.size() > 0) {
                                    for (int i = 0; i < dbList.size(); i++){
                                        idList.add(dbList.get(i).id);
                                    }

                                    for (int i = 0; i < list.size(); i++){
                                        if (!idList.contains(list.get(i).id)){
                                            DatabaseInitializer.addProduct(db,list.get(i));
                                        }
                                    }
                                }else {
                                    for (int i = 0; i < list.size(); i++){
                                        DatabaseInitializer.addProduct(db,list.get(i));
                                    }
                                }
                            }else {
                                for (int i = 0; i < list.size(); i++){
                                    DatabaseInitializer.addProduct(db,list.get(i));
                                }
                            }

                            final int curSize = adapter.getItemCount();
                            myList.addAll(list);

                            rvItems.post(new Runnable() {
                                @Override
                                public void run() {
                                    adapter.notifyItemRangeInserted(curSize, myList.size() - 1);
                                }
                            });
                        }


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: " + error.toString() );


                List<Item> alist = getTenFromRoom(page);

                list.addAll(alist);

                final int curSize = adapter.getItemCount();
                myList.addAll(list);

                rvItems.post(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyItemRangeInserted(curSize, myList.size() - 1);
                    }
                });
            }
        });

        queue.add(request);
    }


    //Parse for permissions
    private String parseForPermissions(JSONObject jsonObject) throws JSONException{

        String data = "";

        Permissions permissions = new Permissions();

        permissions.admin = jsonObject.getBoolean("admin");
        permissions.pull = jsonObject.getBoolean("pull");
        permissions.push = jsonObject.getBoolean("push");

        if (permissions.admin){
            data = data + ", admin";
        }

        if (permissions.pull){
            data = data + ", pull";
        }
         if (permissions.push){
            data = data + ", push";
        }

        data = data.substring(2,data.length());

        return data;
    }

    private void hitApiToFetchData(int pageNumber, final RecyclerView view) {
        myList = new ArrayList<>();
        queue = Volley.newRequestQueue(MainActivity.this);

        String url = "https://api.github.com/orgs/octokit/repos?page="+pageNumber+"&per_page=10";

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e(TAG, "onResponse: " + response );

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    if (jsonArray !=  null){
                        if (jsonArray.length() > 0){
                            for (int i = 0; i < jsonArray.length(); i++){
                                Item item = new Item();


                                //parse

                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                item.id = jsonObject.getInt("id");
                                item.name = jsonObject.getString("name");
                                item.description = jsonObject.getString("description");
                                if (!jsonObject.isNull("license")) {
                                    item.license_name = (new JSONObject(jsonObject.getString("license"))).getString("name");
                                }else {
                                    item.license_name = "No license";
                                }

                                item.open_issues_count = jsonObject.getInt("open_issues_count");
                                item.permissions = parseForPermissions(new JSONObject(jsonObject.getString("permissions")));

                                myList.add(item);

                            }

                            List<Integer> idList = new ArrayList<>();
                            List<Item> dbList = db.userDao().getAll();
                            if (dbList != null) {
                                if (dbList.size() > 0) {
                                    for (int i = 0; i < dbList.size(); i++){
                                        idList.add(dbList.get(i).id);
                                    }

                                    for (int i = 0; i < myList.size(); i++){
                                        if (!idList.contains(myList.get(i).id)){
                                            DatabaseInitializer.addProduct(db,myList.get(i));
                                        }
                                    }
                                }else {
                                    for (int i = 0; i < myList.size(); i++){
                                        DatabaseInitializer.addProduct(db,myList.get(i));
                                    }
                                }
                            }else {
                                for (int i = 0; i < myList.size(); i++){
                                    DatabaseInitializer.addProduct(db,myList.get(i));
                                }
                            }

                            adapter = new CustomRecyclerViewAdapter(myList);
                            rvItems.setAdapter(adapter);

                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this);
                            rvItems.setLayoutManager(linearLayoutManager);
                            // Retain an instance so that you can call `resetState()` for fresh searches
                            scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
                                @Override
                                public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                                    // Triggered only when new data needs to be appended to the list
                                    // Add whatever code is needed to append new items to the bottom of the list

                                    Log.e(TAG, "onLoadMore: " + page );

                                    createFirstList(page);

                                }
                            };
                            // Adds the scroll listener to RecyclerView
                            rvItems.addOnScrollListener(scrollListener);

                        }

                    }
                } catch (JSONException e) {
                    Log.e(TAG, "onResponse: " + e.toString() );
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: " + error.toString() );

                myList = getTenFromRoom(0);

                Log.e(TAG, "onErrorResponse: aaaaaa" );
                if (myList != null){
                    adapter = new CustomRecyclerViewAdapter(myList);
                    rvItems.setAdapter(adapter);

                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this);
                    rvItems.setLayoutManager(linearLayoutManager);
                    // Retain an instance so that you can call `resetState()` for fresh searches
                    scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
                        @Override
                        public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                            // Triggered only when new data needs to be appended to the list
                            // Add whatever code is needed to append new items to the bottom of the list

                            Log.e(TAG, "onLoadMore: " + page );

                            createFirstList(page);

                        }
                    };
                    // Adds the scroll listener to RecyclerView
                    rvItems.addOnScrollListener(scrollListener);
                }
            }
        });

        queue.add(request);
    }

    private List<Item> getTenFromRoom(int index) {

        Log.e(TAG, "getTenFromRoom: " + index );

        List<Item> itemList = new ArrayList<>();

        List<Item> dbList = db.userDao().getAll();
        if (dbList != null) {
            if (dbList.size() > 0) {

                if (dbList.size() < ((index+1)*10)){
                    for (int i = index*10; i < dbList.size(); i++){
                        itemList.add(dbList.get(i));
                    }
                }else{
                    for (int i = index*10; i < (index+1)*10; i++){
                        itemList.add(dbList.get(i));
                    }
                }

            }else {
              itemList = null;
            }
        }else {
           itemList = null;
        }

        return itemList;
    }

    public void checkPermission(String permission, int requestCode)
    {
        if (ContextCompat.checkSelfPermission(MainActivity.this, permission)
                == PackageManager.PERMISSION_DENIED) {

            // Requesting the permission
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[] { permission },
                    requestCode);
        }
        else {
            Toast.makeText(MainActivity.this,
                    "Permission already granted",
                    Toast.LENGTH_SHORT)
                    .show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults)
    {
        super
                .onRequestPermissionsResult(requestCode,
                        permissions,
                        grantResults);
        if (requestCode == 101) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(MainActivity.this,
                        "Storage Permission Granted",
                        Toast.LENGTH_SHORT)
                        .show();
            }
            else {
                finish();
                Toast.makeText(MainActivity.this,
                        "Storage Permission Denied",
                        Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }
}
