package com.example.shivamsinghalsbnri;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class CustomRecyclerViewAdapter extends RecyclerView.Adapter<CustomRecyclerViewAdapter.ViewHolder> {
    private List<Item> lists;

    public CustomRecyclerViewAdapter(List<Item> list) {
        lists = list;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_open_issues_count;
        public TextView tv_license_name;
        public TextView tv_permissions;
        public TextView tv_name;
        public TextView tv_description;
        public ViewHolder(View itemView) {
            super(itemView);

            tv_open_issues_count = (TextView) itemView.findViewById(R.id.tv_open_issues_count);
            tv_license_name = (TextView) itemView.findViewById(R.id.tv_license_name);
            tv_permissions = (TextView) itemView.findViewById(R.id.tv_permissions);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            tv_description = (TextView) itemView.findViewById(R.id.tv_description);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View contactView = inflater.inflate(R.layout.item, parent, false);

        ViewHolder viewHolder = new ViewHolder(contactView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {


//        Log.e("AAAAAAAAAAA", "onBindViewHolder: " + lists.get(0).owner_id );

        Item item = lists.get(position);

        TextView tv_open_issues_count = viewHolder.tv_open_issues_count;
        TextView tv_license_name = viewHolder.tv_license_name;
        TextView tv_permissions = viewHolder.tv_permissions;
        TextView tv_name = viewHolder.tv_name;
        TextView tv_description = viewHolder.tv_description;
        tv_open_issues_count.setText(""+item.open_issues_count);
        tv_description.setText(item.description);
        tv_license_name.setText(item.license_name);
        tv_name.setText(item.name);
        tv_permissions.setText(item.permissions);
    }

    @Override
    public int getItemCount() {

        return lists.size();
    }
}
