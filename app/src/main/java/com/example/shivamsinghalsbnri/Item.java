package com.example.shivamsinghalsbnri;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Item")
public class Item {

    @PrimaryKey(autoGenerate = true)
    public int _id;

    @ColumnInfo(name = "id")
    public int id;

    @ColumnInfo(name = "open_issues_count")
    public int open_issues_count;

    @ColumnInfo(name = "name")
    public String name;

    @ColumnInfo(name = "permissions")
    public String permissions;

    @ColumnInfo(name = "license_name")
    public String license_name;

    @ColumnInfo(name = "description")
    public String description;
}
